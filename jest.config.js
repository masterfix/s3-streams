/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['test'],
  collectCoverageFrom: ['src/**/*.ts'],
  setupFiles: ['dotenv/config'],
  reporters: ['default', 'jest-junit'],
  coverageReporters: ['text', 'cobertura'],
};
