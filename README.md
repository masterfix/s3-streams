# s3-streams

A node library which simplifies uploads and downloads of S3 objects.

## Installation

```bash
npm install @masterfix/s3-streams
```

## Example

```javascript
import {S3Client} from '@aws-sdk/client-s3';
import {createS3ReadStream} from '@masterfix/s3-streams';
import {createWriteStream} from 'fs';

const s3Client = new S3Client({
  region: 'US',
  endpoint: 'https://eu-central-1.linodeobjects.com',
  credentials: {
    accessKeyId: 'topsecret',
    secretAccessKey: 'topsecret',
  },
});

const readStream = createS3ReadStream({
  s3Client,
  bucket: 'mybucket',
  key: 'remote-file.tar.gz',
});

const writeStream = createWriteStream('local-file.tar.gz');

readStream.pipe(writeStream);
```

## Running Tests

To run tests, run the following command:

```bash
npm test
```

## Environment Variables

To run the tests, you will need to add the following environment variables to your .env file:

`S3_REGION`
`S3_ENDPOINT`
`S3_ACCESS_KEY`
`S3_SECRET_KEY`
`S3_BUCKET`

## License

[MIT](https://choosealicense.com/licenses/mit/)
