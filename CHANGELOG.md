# [1.4.0](https://gitlab.com/masterfix/s3-streams/compare/v1.3.0...v1.4.0) (2023-05-24)


### Features

* optional passing of metadata ([4f4c4f9](https://gitlab.com/masterfix/s3-streams/commit/4f4c4f911d21df696fcc698dcba1305193deb3a8))

# [1.3.0](https://gitlab.com/masterfix/s3-streams/compare/v1.2.0...v1.3.0) (2023-04-20)


### Features

* add the possibility to set mime type ([860f73c](https://gitlab.com/masterfix/s3-streams/commit/860f73c71b29c890259ede2e8e6cd51255d452ef))

# [1.2.0](https://gitlab.com/masterfix/s3-streams/compare/v1.1.0...v1.2.0) (2023-04-20)


### Features

* add possibility to create public objects ([bd3a927](https://gitlab.com/masterfix/s3-streams/commit/bd3a9277579a128937f7258284f64b7eba0755f7))

# [1.1.0](https://gitlab.com/masterfix/s3-streams/compare/v1.0.7...v1.1.0) (2023-04-20)


### Features

* fix reading big files ([3ec55b1](https://gitlab.com/masterfix/s3-streams/commit/3ec55b125a248950d0387025e1e6d39dd820ac25))

## [1.0.7](https://gitlab.com/masterfix/s3-streams/compare/v1.0.6...v1.0.7) (2023-04-20)


### Bug Fixes

* next try to fix the coverage feature ([f2a26fd](https://gitlab.com/masterfix/s3-streams/commit/f2a26fd21d192466e6517af49e211b9deb8a560c))

## [1.0.6](https://gitlab.com/masterfix/s3-streams/compare/v1.0.5...v1.0.6) (2023-04-19)


### Bug Fixes

* update package-lock.json automatically too ([d98c955](https://gitlab.com/masterfix/s3-streams/commit/d98c955c5909d69867c4ba5571457a316e7e2c16))

## [1.0.5](https://gitlab.com/masterfix/s3-streams/compare/v1.0.4...v1.0.5) (2023-04-19)


### Bug Fixes

* force new release ([4fed485](https://gitlab.com/masterfix/s3-streams/commit/4fed485f0047071f2914a0cb01973d0376d5a438))

## [1.0.4](https://gitlab.com/masterfix/s3-streams/compare/v1.0.3...v1.0.4) (2023-04-19)


### Bug Fixes

* more semantic-release tweaks ([edebc58](https://gitlab.com/masterfix/s3-streams/commit/edebc58bc7ee2b1c7bcb3e7ea4953e38fd969d5c))
