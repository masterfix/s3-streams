import {GetObjectCommand, S3Client} from '@aws-sdk/client-s3';
import {PassThrough, Readable} from 'stream';

export type S3MetaInfo = {
  md5: string;
};

export type S3ReadStream = S3Readable & {
  on(
    event: 'meta-info',
    listener: (metaInfo: S3MetaInfo) => void
  ): S3ReadStream;
};

export type S3ReadStreamOptions = {
  s3Client: S3Client;
  bucket: string;
  key: string;
};

export function createS3ReadStream(opts: S3ReadStreamOptions): S3ReadStream {
  return new S3Readable(opts);
}

class S3Readable extends Readable {
  private firstRead = true;
  private sourceStream = new PassThrough();

  constructor(private readonly opts: S3ReadStreamOptions) {
    super();
  }

  _read(): void {
    if (this.firstRead) {
      this.firstRead = false;
      this.initializeSource();
    } else {
      this.sourceStream.resume();
    }
  }

  private initializeSource() {
    this.opts.s3Client
      .send(
        new GetObjectCommand({
          Bucket: this.opts.bucket,
          Key: this.opts.key,
        })
      )
      .then(response => {
        if (!(response.Body instanceof Readable)) {
          this.destroy(new Error('body is not a readable stream'));
          return;
        }

        this.sourceStream.on('data', chunk => {
          if (!this.push(chunk)) {
            this.sourceStream.pause();
          }
        });
        this.sourceStream.on('end', () => {
          this.push(null);
        });
        this.sourceStream.on('error', error => {
          this.destroy(error);
        });

        response.Body.pipe(this.sourceStream);
      })
      .catch(error => this.destroy(error));
  }
}
