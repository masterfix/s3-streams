import {S3Client} from '@aws-sdk/client-s3';
import {Upload} from '@aws-sdk/lib-storage';
import {EventEmitter, PassThrough, Writable} from 'stream';

export type S3UploadInfo = {
  etag: string | undefined;
  location: string | undefined;
};

export type S3WriteStream = S3Writable & {
  on(
    event: 'upload-info',
    listener: (uploadInfo: S3UploadInfo) => void
  ): S3WriteStream;
};

export type S3WriteStreamOptions = {
  s3Client: S3Client;
  bucket: string;
  key: string;
  public?: boolean;
  mime?: string;
  metaData?: Record<string, string>;
};

export function createS3WriteStream(opts: S3WriteStreamOptions): S3WriteStream {
  return new S3Writable(opts);
}

const UPLOAD_FINISHED_EVENT = 'upload-finished';

class S3Writable extends Writable {
  private destStream: PassThrough | undefined;
  private eventEmitter = new EventEmitter();

  constructor(private readonly opts: S3WriteStreamOptions) {
    super();
  }

  _write(
    chunk: unknown,
    encoding: BufferEncoding,
    callback: (error?: Error | null | undefined) => void
  ): void {
    if (!this.destStream) {
      this.destStream = new PassThrough();

      new Upload({
        client: this.opts.s3Client,
        params: {
          Bucket: this.opts.bucket,
          Key: this.opts.key,
          Body: this.destStream,
          ACL: this.opts.public ? 'public-read' : 'private',
          ContentType: this.opts.mime ? this.opts.mime : undefined,
          Metadata: this.opts.metaData,
        },
      })
        .done()
        .then(() => this.eventEmitter.emit(UPLOAD_FINISHED_EVENT))
        .catch(error => this.destroy(error));
    }

    this.destStream.write(chunk, encoding, error => {
      callback(error);
    });
  }

  _final(callback: (error?: Error | null | undefined) => void): void {
    if (this.destStream) {
      this.destStream.end(() => {
        this.eventEmitter.once(UPLOAD_FINISHED_EVENT, () => {
          callback();
        });
      });
    }
  }
}
