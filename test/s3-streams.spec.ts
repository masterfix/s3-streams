import {S3Client} from '@aws-sdk/client-s3';
import {createHash} from 'crypto';
import {createReadStream, createWriteStream} from 'fs';
import {env} from 'process';
import {createS3ReadStream} from '../src/s3-read-stream';
import {createS3WriteStream} from '../src/s3-write-stream';

describe('S3ReadStream and S3WriteStream combined', () => {
  let s3Client: S3Client;

  let bucket: string;

  beforeAll(() => {
    s3Client = new S3Client({
      region: env.S3_REGION || '',
      endpoint: env.S3_ENDPOINT || '',
      credentials: {
        accessKeyId: env.S3_ACCESS_KEY || '',
        secretAccessKey: env.S3_SECRET_KEY || '',
      },
    });
    bucket = env.S3_BUCKET || '';
  });

  test('compare hash of uploaded and downloaded file', done => {
    const testFile = 'test/data/core.files.tar.gz';
    const finalFile = 'test/data/pkgs/core.files.tar.gz.test';
    const key = `tests/file-${jest.getSeed()}.tar.gz`;

    let firstHash: string;
    let secondHash: string;

    createReadStream(testFile)
      .pipe(createHash('md5').setEncoding('hex'))
      .on('data', hash => {
        firstHash = hash;
      })
      .on('finish', () => {
        createReadStream(testFile)
          .pipe(createS3WriteStream({s3Client, bucket, key}))
          .on('error', error => {
            done(error);
          })
          .on('finish', () => {
            createS3ReadStream({s3Client, bucket, key})
              .on('error', error => {
                done(error);
              })
              .pipe(createWriteStream(finalFile))
              .on('finish', () => {
                createReadStream(finalFile)
                  .pipe(createHash('md5').setEncoding('hex'))
                  .on('data', hash => {
                    secondHash = hash;
                  })
                  .on('finish', () => {
                    try {
                      expect(secondHash).toBe(firstHash);
                      done();
                    } catch (error) {
                      done(error);
                    }
                  });
              });
          });
      });
  }, 30000);
});
