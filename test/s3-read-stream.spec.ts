import {S3Client} from '@aws-sdk/client-s3';
import {createHash} from 'crypto';
import {createReadStream, createWriteStream} from 'fs';
import {env} from 'process';
import {createS3ReadStream} from '../src/s3-read-stream';

describe('S3ReadStream', () => {
  let s3Client: S3Client;

  let bucket: string;

  beforeAll(() => {
    s3Client = new S3Client({
      region: env.S3_REGION || '',
      endpoint: env.S3_ENDPOINT || '',
      credentials: {
        accessKeyId: env.S3_ACCESS_KEY || '',
        secretAccessKey: env.S3_SECRET_KEY || '',
      },
    });
    bucket = env.S3_BUCKET || '';
  });

  test('read medium file', done => {
    createS3ReadStream({s3Client, bucket, key: 'core.files.tar.gz'})
      .on('error', error => {
        done(error);
      })
      .on('end', () => {
        done();
      })
      .resume();
  });

  test('read medium file and save it to a local file', done => {
    createS3ReadStream({s3Client, bucket, key: 'core.files.tar.gz'})
      .on('error', error => {
        done(error);
      })
      .pipe(createWriteStream('test/data/core.files.tar.gz.copy.test'))
      .on('finish', () => {
        done();
      });
  });

  test('read big file and save it to a local file', done => {
    const inputFile = 'tests/JetBrainsMono.zip';
    const outputFile = 'test/data/JetBrainsMono.zip.test';

    createS3ReadStream({s3Client, bucket, key: inputFile})
      .on('error', error => {
        done(error);
      })
      .pipe(createWriteStream(outputFile))
      .on('finish', () => {
        createReadStream(outputFile)
          .pipe(createHash('md5').setEncoding('hex'))
          .on('data', hash => {
            try {
              expect(hash).toBe('b92de5fab625fe2b17d88322dde77f90');
              done();
            } catch (error) {
              done(error);
            }
          });
      });
  }, 30000);
});
