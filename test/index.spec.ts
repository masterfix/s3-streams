import {createS3ReadStream, createS3WriteStream} from '../src';

describe('index', () => {
  test('exports createS3ReadStream', () => {
    expect(createS3ReadStream).toBeDefined();
  });

  test('exports createS3WriteStream', () => {
    expect(createS3WriteStream).toBeDefined();
  });
});
