import {S3Client} from '@aws-sdk/client-s3';
import {createReadStream} from 'fs';
import {env} from 'process';
import {createS3WriteStream} from '../src/s3-write-stream';

describe('S3WriteStream', () => {
  let s3Client: S3Client;

  let bucket: string;

  beforeAll(() => {
    s3Client = new S3Client({
      region: env.S3_REGION || '',
      endpoint: env.S3_ENDPOINT || '',
      credentials: {
        accessKeyId: env.S3_ACCESS_KEY || '',
        secretAccessKey: env.S3_SECRET_KEY || '',
      },
    });
    bucket = env.S3_BUCKET || '';
  });

  test('write big file', done => {
    createReadStream('test/data/core.files.tar.gz')
      .pipe(
        createS3WriteStream({
          s3Client,
          bucket,
          key: `file-${jest.getSeed()}.tar.gz`,
        })
      )
      .on('error', error => {
        done(error);
      })
      .on('finish', () => {
        done();
      });
  });
});
